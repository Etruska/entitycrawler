package cz.fit.crawler;

import cz.fit.crawler.controller.Controller;
import cz.fit.crawler.model.Model;
import cz.fit.crawler.view.AppLayout;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import javax.swing.SwingUtilities;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) throws Exception {

        final Model model = new Model();
        final Controller myController = new Controller(model);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                new AppLayout(model, myController).setVisible(true);
            }
        });

    }
}
