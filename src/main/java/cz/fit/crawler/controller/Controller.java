package cz.fit.crawler.controller;

import cz.fit.crawler.model.Crawler;
import cz.fit.crawler.model.CrawlerController;
import cz.fit.crawler.model.CrawlerObserver;
import cz.fit.crawler.model.Model;
import cz.fit.crawler.model.WebEntity;
import cz.fit.crawler.view.AppLayout;
import cz.fit.crawler.view.CrawlerView;
import cz.fit.crawler.view.View;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import static javax.swing.SwingWorker.StateValue.DONE;

/**
 * Headquarters module Controller
 *
 * @author Vojtěch Petrus
 */
public final class Controller implements CrawlerObserver {

    private Model model;
    private AppLayout appLayout;
    private CrawlerView crawlerView;

    public Controller(Model model) {
        this.model = model;
        Crawler.init();
    }

    private void initViews() {
        this.crawlerView = new CrawlerView(this, model);
    }

    public void submitButtonClicked(String seedUrl, String minorType, String majorType, String entityName, int maxPages) {

        try {
            validateInputs(seedUrl, minorType, majorType, entityName, maxPages);
            appLayout.showStatus("Started crawling at url " + seedUrl, AppLayout.STATUS_INFO);
            model.setSeed(seedUrl);

            CrawlerController cc = model.createCrawlerController();
            Crawler.attachObserver(this);

            model.setMaxPages(maxPages);
            model.runCrawler(minorType, majorType, entityName);
            cc.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(final PropertyChangeEvent event) {
                    if ("progress".equals(event.getPropertyName())) {

                    } else if ("state".equals(event.getPropertyName())) {
                        if ((SwingWorker.StateValue) event.getNewValue() == DONE) {
                            appLayout.showStatus("Crawling finished.", AppLayout.STATUS_OK);
                        }
                    }
                }
            }
            );
        } catch (IllegalArgumentException ex) {
            appLayout.showStatus(ex.getMessage(), AppLayout.STATUS_ERROR);
        }

    }
 
    public void validateInputs(String seedUrl, String minorType, String majorType, String entityName, int maxPages) throws IllegalArgumentException {

        if (seedUrl.isEmpty()) {
            throw new IllegalArgumentException("URL cannot be empty.");
        }
        if (minorType.isEmpty() && majorType.isEmpty()) {
            throw new IllegalArgumentException("Please fill either minor or major type.");
        }
        if (entityName.isEmpty()) {
            throw new IllegalArgumentException("Entity name cannot be empty.");
        }
        if (maxPages < 1 || maxPages > 10000) {
            throw new IllegalArgumentException("Max pages must be between 1 and 10000.");
        }

    }

    /**
     * Called when application layout is ready to be rendered
     *
     * @param layout main application frame
     */
    public void setAppLayout(AppLayout layout) {
        this.appLayout = layout;
        this.initViews();
        this.crawlerView.setAppLayout(layout);
    }

    public View getContentPanel() {
        return crawlerView;
    }

    public void pageVisited(String url, List<WebEntity> entities) {

        System.out.println("Page visited: " + url);
        appLayout.showStatus("Visited " + url + ".", AppLayout.STATUS_OK);
    }

}
