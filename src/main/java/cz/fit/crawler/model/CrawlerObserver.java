/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.fit.crawler.model;

import java.util.List;

/**
 *
 * @author etruska
 */
public interface CrawlerObserver {
    
    public void pageVisited(String url, List<WebEntity> entities);
}
