package cz.fit.crawler.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Table model for entity JTable
 *
 * @author Vojtěch Petrus
 */
public class EntityTableModel extends AbstractTableModel {

    private Model model;
    private List<WebEntity> entities;

    private String[] columnNames = {"Name",
        "Type",
        "URL"};

    public EntityTableModel(Model model) {
        this.model = model;
        this.entities = new ArrayList<WebEntity>();
    }

    @Override
    public int getRowCount() {
        return entities.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    public WebEntity getRow(int rowIndex) {
        if (rowIndex < entities.size()) {
            return entities.get(rowIndex);
        }
        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        WebEntity e = entities.get(rowIndex);
        Object value = "?";
        switch (columnIndex) {
            case 0:
                value = e.getName();
                break;
            case 1:
                value = e.getType();
                break;
            case 2:
                value = e.getUrl();
                break;
        }
        return value;
    }

    public void addEntities(List<WebEntity> e) {
        this.entities.addAll(e);
    }
    
    public void clearAll(){
        this.entities.clear();
    }

}
