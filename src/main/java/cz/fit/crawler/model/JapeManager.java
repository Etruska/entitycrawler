/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.crawler.model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author etruska
 */
public class JapeManager {

    private final String template = "Phase:firstpass\n"
            + "Input:  Lookup Token\n"
            + "Rule: TwoPatterns\n"
            + "Priority: 20\n"
            + "(\n"
            + "{__rules__}\n"
            + "): country\n"
            + "-->\n"
            + ":country.__name__ ={rule= \"TwoPatterns\" }";

    private String storageFile = "data";
    private String resultFile = "new.jape";

    public void createJape(String minorType, String majorType, String name) {
        PrintWriter writer = null;
        try {
            String newJape = template.replace("__name__", name);

            String rules = "";
            if (!minorType.isEmpty()) {
                rules = getMinorType(minorType);
                if (!majorType.isEmpty()) {
                    rules += ",";
                }
            }
            if (!majorType.isEmpty()) {
                rules += getMajorType(majorType);
            }

            newJape = newJape.replace("__rules__", rules);

            writer = new PrintWriter(storageFile + "/" + resultFile, "UTF-8");
            writer.print(newJape);
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JapeManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(JapeManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
    }

    private String getMinorType(String type) {
        return "Lookup.minorType == \"" + type + "\"";
    }

    private String getMajorType(String type) {
        return "Lookup.majorType == \"" + type + "\"";
    }

}
