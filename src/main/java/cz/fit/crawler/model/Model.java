package cz.fit.crawler.model;

/**
 * Model class, communicates with DAO classes and informs its observers about
 * changes
 *
 * @author Vojtěch Petrus
 */
public class Model {

    private CrawlerController crawlerController;
    private JapeManager japeManager;

    private String minorType = "";
    private String majorType = "";
    private String entityName = "";
    private String seedUrl;
    private int maxPages;

    public Model() {
        japeManager = new JapeManager();
    }

    public void createJape(String minorType, String majorType, String entityName) {
        this.minorType = minorType;
        this.majorType = majorType;
        this.entityName = entityName;
        japeManager.createJape(minorType, majorType, entityName);
    }

    public void setSeed(String url) {
       this.seedUrl = url;
    }

    public void runCrawler(String minorType, String majorType, String entityName) {
        System.out.println("run crawler");
        crawlerController.setMaxPages(maxPages);
        crawlerController.addSeed(seedUrl);
        this.createJape(minorType, majorType, entityName);
        Crawler.entityName = this.entityName;
        crawlerController.execute();
    }

    public CrawlerController createCrawlerController() {
        crawlerController = new CrawlerController();
        return crawlerController;
    }

    public void setMaxPages(int maxPages) {
        this.maxPages = maxPages;
    }

}
