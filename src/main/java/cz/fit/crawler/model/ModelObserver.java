package cz.fit.crawler.model;

/**
 * Interface for all objects listening to model changes
 * @author Vojtěch Petrus
 */
public interface ModelObserver {
    
    public void modelUpdated();
    
}
