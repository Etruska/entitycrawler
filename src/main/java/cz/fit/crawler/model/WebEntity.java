/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.fit.crawler.model;

/**
 *
 * @author etruska
 */
public class WebEntity {
    
    private String url;
    private String type;
    private String name;

    public WebEntity(String url, String type, String name) {
        this.url = url;
        this.type = type;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    
    
}
