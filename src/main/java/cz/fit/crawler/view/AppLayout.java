package cz.fit.crawler.view;

import cz.fit.crawler.controller.Controller;
import cz.fit.crawler.model.Model;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Main window with application layout
 *
 * @author Vojtěch Petrus
 */
public class AppLayout extends JFrame {

    private Model model;
    private Controller controller;
    private JPanel contentBoard;
    private JLabel statusBar;
    private CardLayout boardLayout;

    public static final int STATUS_OK = 1;
    public static final int STATUS_ERROR = 2;
    public static final int STATUS_INFO = 3;

    List<View> contentPanels = new ArrayList<View>();

    public AppLayout(Model model, Controller controller) {
        try {

            this.model = model;
            this.controller = controller;

            controller.setAppLayout(this);

            this.setLayout(new BorderLayout());

            this.setSize(new Dimension(ViewConfig.WINDOW_WIDTH, ViewConfig.WINDOW_HEIGHT));
            int[] position = getCenterPosition(ViewConfig.WINDOW_WIDTH, ViewConfig.WINDOW_HEIGHT);
            this.setLocation(position[0], position[1]);

            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setTitle("Entity Crawler");
            this.setResizable(true);

            this.addContentView(controller.getContentPanel());

            this.statusBar = new JLabel("EntityCrawler v. 0.5");
            this.statusBar.setName("stausBar");
            this.statusBar.setBackground(Color.white);
            this.statusBar.setOpaque(true);
            this.statusBar.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            this.add(this.statusBar, BorderLayout.SOUTH);

        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }

    }

    public void addContentView(View view) {
        this.add(view, BorderLayout.CENTER);
    }

    public void modelUpdated() {
    }

    public void showStatus(String text, int type) {
        this.statusBar.setText(text);

        Font font = this.statusBar.getFont();
        Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
        this.statusBar.setFont(boldFont);

        switch (type) {
            case STATUS_OK:
                this.statusBar.setForeground(ViewConfig.COLOR_GREEN);
                break;
            case STATUS_ERROR:
                this.statusBar.setForeground(ViewConfig.COLOR_RED);
                break;
            case STATUS_INFO:
                this.statusBar.setForeground(ViewConfig.COLOR_BLUE);
                break;
        }
        if (false) {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    clearStatus();
                }
            }, ViewConfig.STATUS_DURATION
            );
        }
    }

    public String getStatus() {
        return this.statusBar.getText();
    }

    public void clearStatus() {
        this.statusBar.setText(" ");
    }

    public static int[] getCenterPosition(int width, int height) {
        int[] position = new int[2];
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

        position[0] = (int) (screen.getWidth() / 2 - (width / 2));
        position[1] = (int) (screen.getHeight() / 2 - (height / 2));
        return position;
    }
}
