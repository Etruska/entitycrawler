package cz.fit.crawler.view;

import cz.fit.crawler.controller.Controller;
import cz.fit.crawler.model.Crawler;
import cz.fit.crawler.model.CrawlerObserver;
import cz.fit.crawler.model.EntityTableModel;
import cz.fit.crawler.model.Model;
import cz.fit.crawler.model.WebEntity;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author Vojtěch Petrus
 */
public class CrawlerView extends View implements CrawlerObserver {

    private JButton startButton;
    private JButton clearButton;
    private JTextField urlField;
    private TextFieldWithPrompt minorField;
    private TextFieldWithPrompt majorField;
    private TextFieldWithPrompt entityName;
    private TextFieldWithPrompt maxPages;
    private EntityTable entityTable;
    private EntityTableModel entityTableModel;
    private AppLayout appLayout;
    private Controller controller;
    private Model model;

    public void setAppLayout(AppLayout layout) {
        this.appLayout = layout;
    }

    public CrawlerView(Controller con, Model model) {
        this.controller = con;
        this.model = model;
        this.startButton = new JButton("Run crawler");
        this.clearButton = new JButton("Clear results");
        this.entityTableModel = new EntityTableModel(model);
        this.entityTable = new EntityTable(entityTableModel, this);
        this.urlField = new JTextField("http://", 40);
        this.urlField.setColumns(30);
        this.minorField = new TextFieldWithPrompt();
        this.minorField.setPrompt("minor type");
        this.minorField.setColumns(20);
        this.majorField = new TextFieldWithPrompt();
        this.majorField.setPrompt("major type");
        this.majorField.setColumns(20);
        this.entityName = new TextFieldWithPrompt();
        this.entityName.setPrompt("entity name");
        this.entityName.setColumns(20);
        this.maxPages = new TextFieldWithPrompt();
        this.maxPages.setPrompt("max pages");

        this.setLayout(new GridBagLayout());

        Crawler.attachObserver(this);
        initComponents();
        setHandlers();
    }

    private void initComponents() {
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(0, 10, 0, 10);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.3;
        this.add(this.minorField, c);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.5;
        this.add(this.majorField, c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 0.5;
        this.add(this.entityName, c);

        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.05;
        this.add(new JLabel("Starting URL:"), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 3;
        c.weightx = 0.9;
        this.add(this.urlField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 1;
        c.gridwidth = 1;
        c.weightx = 0.3;
        this.add(this.maxPages, c);

        c.gridx = 5;
        c.gridy = 1;
        c.weightx = 0.1;
        this.add(this.startButton, c);

        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0;
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 6;
        c.ipady = 300;
        this.entityTable.setPreferredScrollableViewportSize(new Dimension(400, 200));
        this.entityTable.setFillsViewportHeight(true);
        this.add(new JScrollPane(this.entityTable), c);

        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0.1;
        c.gridwidth = 1;
        c.ipady = 0;
        this.add(this.clearButton, c);
    }

    private void setHandlers() {
        this.startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int pages;
                try {
                    pages = Integer.parseInt(maxPages.getText());
                } catch (NumberFormatException e) {
                    pages = 0;
                }
                controller.submitButtonClicked(urlField.getText(), minorField.getText(), majorField.getText(), entityName.getText(), pages);
            }
        });

        this.clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                entityTableModel.clearAll();
                entityTableModel.fireTableDataChanged();
                revalidate();
            }
        });

    }

    @Override
    public String getViewName() {
        return "CrawlerView";
    }

    public void pageVisited(String url, List<WebEntity> entities) {
        System.out.println("CV: pageVisited - " + url);
        this.entityTableModel.addEntities(entities);
        this.entityTableModel.fireTableDataChanged();
        this.revalidate();
    }

}
