package cz.fit.crawler.view;

import cz.fit.crawler.model.EntityTableModel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author etruska
 */
public class EntityTable extends JTable {

    EntityTableModel tableModel;
    CrawlerView view;

    public EntityTable(EntityTableModel tableModel, CrawlerView view) {
        super(tableModel);
        this.tableModel = tableModel;
        this.view = view;
        this.setBounds(0, 0, 950, 600);
        this.setHandlers();
    }

    private void setHandlers() {
        this.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (event.getValueIsAdjusting()) {
                    return;
                }

            }
        });
    }

}
