/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.crawler.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import javax.swing.FocusManager;
import javax.swing.JTextField;

/**
 *
 * @author etruska
 */
public class TextFieldWithPrompt extends JTextField {

    private String prompt = "fill";

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    @Override
    protected void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);

        if (getText().isEmpty() && !(FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == this)) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setBackground(new Color(200,200,200));
            g2.setColor(Color.GRAY);
            g2.drawString(this.prompt, 7, 18); //figure out x, y from font's FontMetrics and size of component.
            g2.dispose();
        }
    }
}
