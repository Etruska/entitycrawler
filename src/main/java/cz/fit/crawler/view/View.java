package cz.fit.crawler.view;

import javax.swing.JPanel;


/**
 *  Abstract view class that all content views must extend
 * @author Vojtěch Petrus
 */
public abstract class View extends JPanel{
    
    public abstract String getViewName();
}
