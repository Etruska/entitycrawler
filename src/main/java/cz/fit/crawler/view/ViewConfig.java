package cz.fit.crawler.view;

import java.awt.Color;

/**
 * Static configuration of visible components
 * @author Vojtěch Petrus
 */
public class ViewConfig {
    public static final int WINDOW_WIDTH = 900;
    public static final int WINDOW_HEIGHT = 700;
    public static final int STATUS_DURATION = 10000;
    //i don't like the default green
    public static final Color COLOR_GREEN = new Color(121,196,48);
    public static final Color COLOR_BLUE = Color.BLUE;
    public static final Color COLOR_RED = Color.RED;
}
